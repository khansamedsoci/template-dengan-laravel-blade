<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index() {

        $questions = DB::table('pertanyaan')->get(); //mirip select* from
        return view('pertanyaan.index', compact('questions'));
    }

    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $request->validate([
            "judul" =>"required",
            "isiPertanyaan" => "required"
        ]);
        $query = DB:: table('pertanyaan')->insert([
                "judul" => $request["judul"],
                "isi" => $request["isiPertanyaan"]
        ]);

        return redirect('/pertanyaan')->with('success', 'New Question Has Been Created!');
    }

    public function show($pertanyaan_id){
        $questions = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first(); //ambil data cuma satu karena id unik
        // dd($questions);
        return view('pertanyaan.show', compact('questions'));

    }

    public function edit($pertanyaan_id){
        $questions = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first(); //ambil data cuma satu karena id unik
        // dd($questions);
        return view('pertanyaan.edit', compact('questions'));
    }

    public function update($pertanyaan_id, Request $request){
        $request->validate([
            "judul" =>"required",
            "isiPertanyaan" => "required"
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $pertanyaan_id)
                    ->update([
                        "judul" =>$request["judul"],
                        "isi" => $request["isiPertanyaan"]
                    ]);
        return redirect('/pertanyaan')->with('success', 'Question Has Been Updated!');
    }

    public function destroy($pertanyaan_id){
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan')->with('success', 'Question Has Been Deleted!');
    }
}
