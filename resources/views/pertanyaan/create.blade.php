@extends('layoutAdminlte.master')

@section('content')

<div class="ml-4 mt-4 mr-4">
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create New Question</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
    @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="judul">Title</label>
                <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul','') }}" placeholder="Enter Title">
                @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isiPertanyaan">Question</label>
                <input type="text" class="form-control" id="isiPertanyaan" name="isiPertanyaan" value="{{old('isiPertanyaan','')}}" placeholder="Your Question">
                @error('isiPertanyaan')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
</div>



@endsection