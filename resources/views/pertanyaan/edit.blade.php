@extends('layoutAdminlte.master')

@section('content')

<div class="ml-4 mt-4 mr-4">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Question {{$questions->id}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan/{{$questions->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Title</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $questions->judul) }}" placeholder="Enter Title">
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="isiPertanyaan">Question</label>
                    <input type="text" class="form-control" id="isiPertanyaan" name="isiPertanyaan" value="{{old('isiPertanyaan', $questions->isi)}}" placeholder="Your Question">
                    @error('isiPertanyaan')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
</div>

@endsection