@extends('layoutAdminlte.master')

@section('content')
<div class="ml-4 mt-4 mr-4">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Questions Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            <a href="/pertanyaan/create" class="btn btn-primary mb-2">Create New Question</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">No.</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th style="width: 120px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($questions as $key => $questions)

                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $questions->judul }}</td>
                        <td>{{ $questions->isi }}</td>
                        <td style="display: flex">
                            <a href="/pertanyaan/{{$questions->id}}" class="btn btn-info btn-sm mr-2">Show</a>
                            <a href="/pertanyaan/{{$questions->id}}/edit" class="btn btn-default btn-sm mr-2">Edit</a>
                            <form action="/pertanyaan/{{$questions->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">«</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">»</a></li>
            </ul>
        </div>
    </div>
</div>
@endsection