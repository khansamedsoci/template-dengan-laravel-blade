@extends('layoutAdminlte.master')

@section('content')
<div class="ml-4 mt-4 mr-4">
    <h4>{{ $questions->judul }}</h4>
    <h5>{{ $questions->id }}</h5>
    <p>{{ $questions->isi }}</p>
</div>

@endsection